#!/usr/bin/bash

if [[ ${USER} == 'bernard' ]]; then
    pythonRun="python3"
    homeDir="/home/bernard/repoGit/Bateau/TaC/NMEA_utils"
    fileIn="../Traces/echantillonTest.nmea"
else
    pythonRun="/c/Logiciels/Anaconda3/python"
    homeDir="/c/AncienDisqueD/RepoS/Bateau/TaC/NMEA_utils"
    fileIn="../Traces/echantillonTest.nmea"
fi

if [[ -d ${homeDir} && -r ${homeDir}/${fileIn} ]]; then
    cd ${homeDir}
    ##  Ne conserver que le NMEA
    ${pythonRun} ${homeDir}/NMEA_cleaner.py ${homeDir}/${fileIn} \
        1> ${homeDir}/${fileIn}.cleaned \
        2> ${homeDir}/${fileIn}.cleaned.log
        ##  Supprimer les phrases NMEA suivant conditions
        ##  GPS invalide, checksum invalide
        if [[ $? -eq 0 ]]; then
            ${pythonRun} ${homeDir}/NMEA_suppressSentence.py -i ${homeDir}/${fileIn}.cleaned --invalidRMC --GLL --AIS --DBT --DPT --HDG \
                1> ${homeDir}/${fileIn}.suppressSentence \
                2> ${homeDir}/${fileIn}.suppressSentence.log
        else
            echo "Pb avec suppressSentence"
            exit 1
        fi
else
    echo "Pb avec cleaner" > /dev/stderr
    exit 1
fi



exit 0

