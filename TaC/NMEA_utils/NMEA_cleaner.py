#!/python
# -*- coding: utf-8 -*-
from __future__ import print_function
import os, sys, glob, re, argparse

"""
Ne conserve que le NMEA du fichier passe en parametre
"""

sep = "\t"

def readInfile(infile) :
    #reNmeaDebut = re.compile(r'[\$|!]{1}')
    #reNmeaFin = re.compile(r'\*[0-9A-Fa-f]{2}') 
    reNmea = re.compile(r'[\$|!]{1}.*\*[0-9A-Fa-f]{2}') 
    f = open(infile, 'r')
    numLine = 0
    for line in f :
        numLine += 1
        ##  Examen de la ligne
        matchNmea = reNmea.search(line)
        if (matchNmea) :
            print(matchNmea.group())
        else :
            printErr("Pb ligne " + str(numLine))
    f.close()


def printErr(*args, **kwargs) :
    print(*args, file=sys.stderr, **kwargs)


def start() :
    parser = argparse.ArgumentParser(
        'File parser',
        description='PyParse - The File Processor',
        epilog='Thank you for choosing PyParse!',
        #add_help=False,
        )
    parser.add_argument('-i', '--infile', required=True, help="Input file for cleaning")
    #parser.add_argument('-o', '--outfile', help="Cleaned output file")
    args = parser.parse_args()
    return args


def mainAvecArgparse():
    args = start()
    if args.infile :
        # Traitement
        readInfile(args.infile)
        quit(0)
    #print(args)

def main() :
    # 1 seul argument, le fichier en entree
    nbrArgs = len(sys.argv)
    if (nbrArgs == 2) :
        readInfile(sys.argv[1])
        quit(0)
    else :
        printErr("1 seul argument, le fichier en entree")
        quit(1)

if __name__ == '__main__':
    main()
