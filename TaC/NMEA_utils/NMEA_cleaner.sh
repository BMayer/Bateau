#!/usr/bin/bash

if [[ ${USER} == 'bernard' ]]; then
    pythonRun="python3"
    homeDir="/home/bernard/repoGit/Bateau/TaC/NMEA_utils"
else
    pythonRun="/c/Logiciels/Anaconda3/python"
    homeDir="/c/AncienDisqueD/RepoS/Bateau/TaC/NMEA_utils"
fi


if [[ -d ${homeDir} && -r ${homeDir}/../Traces/echantillonTest.nmea ]]; then
    cd ${homeDir}
    ##  Ne conserver que le NMEA
    # La version avec "argparse" merde avec les redirections
    #${pythonRun} ${homeDir}/NMEA_cleaner.py -i ${homeDir}/../Traces/echantillonTest.nmea
    # version roots
    ${pythonRun} ${homeDir}/NMEA_cleaner.py ${homeDir}/../Traces/echantillonTest.nmea
else
    if [[ ! -d ${homeDir} ]]; then
        echo "Pb avec homedir [${homeDir}]" > /dev/stderr
    fi
    if [[ ! -r ${homeDir}/../Traces/echantillonTest.nmea ]]; then
        echo "Pb avec fileIn [${homeDir}/../Traces/echantillonTest.nmea]" > /dev/stderr
    fi
fi

