#!/python
# -*- coding: utf-8 -*-
from __future__ import print_function
import os, sys, glob, re, argparse
#from decimal import Decimal

"""
Les fonctions de conversion, de coordonnees geographiques, de temps
ubi bene, ibi patria
"""

'''
== Les formats latitude longitude

Le format NMEA des longitudes / latitudes est 4729.9454,N,00228.0680,W  (dd[d]mm.mmmmm)
Le format DMM des longitudes / latitudes est 47.nnnnn, -2.nnnnn
Par exemple, la latitude NMEA 4729.9454,N est à comprendre comme 
DD 47.49909
DMM N47°29.9454
DMS N47°29'56.724"
et la longitude 00228.0692,W est à comprendre comme 
DD -2.46782
DMM W002°28.0692
DMS W2°28'4.152"
2503.6319    : Latitude exprimée en ddmm.mmmmm: 25° 03.6319' = 25° 03' 37,914"
12136.0099  9: Longitude exprimée en dddmm.mmmmm: 121° 36.0099' = 121° 36' 00,594"

NMEA format is ddmm.mmmm, n/s (d)ddmm.mmmm, e/w
To get to decimal degrees from degrees ad minutes, you use the following formula:
(d)dd + (mm.mmmm/60) (* -1 for W and S)

http://www.hiddenvision.co.uk/ez/[]

https://www.numworks.com/fr/professeurs/activites-pedagogiques/snt/trame-gps/[]

https://fr.wikipedia.org/wiki/NMEA_0183[]

https://stackoverflow.com/questions/36254363/how-to-convert-latitude-and-longitude-of-nmea-format-data-to-decimal[]

http://tvaira.free.fr/bts-sn/activites/activite-peripherique-usb/conversions.html[]

https://www.sunearthtools.com/dp/tools/conversion.php?lang=fr[]

http://family.mayer.free.fr/bateau/conversion_DMS_DMM_DD/Copie%20de%20calculators.htm[]

https://www.gpsvisualizer.com/calculators[]

'''

def nmeaChecksumCompute() :
    ## TODO
    ##  https://nmeachecksum.eqth.net/
    ##  $   PFEC,GPint,RMC05   * -> $PFEC,GPint,RMC05*2D
    ##  $   PINFO,Pen Lann 47.51021, -2.50187 --> N47°30.6126 / W002°30.1122,  * -> $PINFO,Pen Lann 47.51021, -2.50187 --> N47°30.6126 / W002°30.1122*22
    ##  https://github.com/semuconsulting/pynmeagps <- Chercher dans ce depot !
    ##  https://code.activestate.com/recipes/576789-nmea-sentence-checksum/ <- plan B
    ##  https://gist.github.com/MattWoodhead/0bc2b3066796e19a3a350689b43b50ab
    ##  https://rietman.wordpress.com/2008/09/25/how-to-calculate-the-nmea-checksum/
    checksum = 0
    ##  Doit etre une string de 2 caracteres
    checksum = ("%02d" % checksum)
    return checksum

def nmeaChecksumValid() :
    ## TODO
    return False

##  Comment savoir si une coordonnées géographique est dans une zone ou pas ?
def isThisPointInZone(point, zones) :
    (latPoint, lonPoint) = point.split(',')
    for zone in zones :
        (zoneNW, zoneSE) = zone.split('|')
        (latNW, lonNW) = zoneNW.split(',')
        (latSE, lonSE) = zoneSE.split(',')
        #print(latPoint, lonPoint, "is in", latNW, lonNW, latSE, lonSE, "?")
        if (float(latPoint) <= float(latNW)) :
             if (float(lonPoint) >= float(lonNW)) :
                if (float(latPoint) >= float(latSE)) :
                    if (float(lonPoint) <= float(lonSE)) :
                        return True
        return False


##  Convertit [0]DDmm.dd,[N|S],[0]DDDmm.dd,[W|E] -> [-]DD.ddddd,[-]DD.ddddd
##  4729.9463,N,00228.0692,W -> 
def convertGeoNmeaToDD(coordonnesNmea) :
    # 4405.2015 —-> 44 + 05.2015/60 = 44,0867 degres
    # Les coordonnees Sud et Ouest seront de valeur negatives.
    ## On recoit 1 paire de coordonnees NMEA a convertir
    ## 4729.9471,N,00228.0705,W   ->   47.51100,-2.413300
    ## 4744.4444,S,00122.333,E    ->   -47.49000,2.36000
    aNmea = coordonnesNmea.split(',')
    nbrTokens = len(aNmea)
    if (nbrTokens == 4) :
        (D, d) = aNmea[0].split('.')
        degres  = D[:-2]
        degres = int(degres)
        minutes = float(D[-2:] + '.' + d) / 60
        sOut = "%.6f" % (degres + minutes)
        if (aNmea[1].upper() == 'S') :
            sOut = '-' + sOut
        (D, d) = aNmea[2].split('.')
        degres  = D[:-2]
        degres = int(degres)
        minutes = float(D[-2:] + '.' + d) / 60
        sOut4 = "%.6f" % (degres + minutes)
        if (aNmea[3].upper() == 'W') :
            sOut4 = '-' + sOut4
        return sOut + r',' + sOut4
    else :
        return False


##  Convertid DD -1.23456 -> 4729.9463,N,00228.0692,W
def convertGeoDDToNmea(coordonneesDD: str) :
    ##  On recoit un float ou 2 ; separes par virgule
    ##  que l'on convertit en DD
    ##  Si 1 seule coordonnee, alors pas moyen de determiner le signe
    coordonneesDD = str(coordonneesDD)
    unOuDeux = coordonneesDD.find(',') # -1 ou 
    if (unOuDeux == -1) :
        DD = coordonneesDD.strip()
        (DDent, DDdec) = DD.split('.')
        DDdec = str(int(DDdec) * 60)
        Nmea = DDent + DDdec[0:2] + '.' + DDdec[2:]
    else :
        (DD, DD2) = coordonneesDD.split(',')
        DD = DD.strip()
        DD2 = DD2.strip()
        (DDent, DDdec) = DD.split('.')
        DDdec = str(int(DDdec) * 60)
        (DD2ent, DD2dec) = DD2.split('.')
        DD2dec = str(int(DD2dec) * 60)
        if (DDent.startswith('-')) :
            DDsign = 'S'
            DDent = DDent[1:]
        else :
            DDsign = 'N'
        if (DD2ent.startswith('-')) :
            DD2sign = 'W'
            DD2ent = DD2ent[1:]
        else :
            DD2sign = 'E'           
        DDent = DDent.zfill(2)
        DD2ent = DD2ent.zfill(3)
        # Ils semblent que les lat / lon soient pourvues de 4 decimales.
        Nmea = DDent + DDdec[0:2] + '.' + DDdec[2:].ljust(4, "0") + ',' + DDsign + ',' + DD2ent + DD2dec[0:2] + '.' + DD2dec[2:].ljust(4, "0") + ',' + DD2sign
    return Nmea


def isInvalidGLL(l) :
    a = l.split(',')
    #print("GLL invalid ?", a[6])
    if (a[6].upper() == 'A') :
        return False
    else :
        return True


def isInvalidRMC(l) :
    a = l.split(',')
    #print("RMC invalid ?", a[2])
    if (a[2].upper() == 'A') :
        return False
    else :
        return True


def printErr(*args, **kwargs) :
    print(*args, file=sys.stderr, **kwargs)

 


def main() :
    print("into NMEA_Fonctions.py")
    sys.exit(1)

if __name__ == '__main__':
    main()
