#!/python
# -*- coding: utf-8 -*-
from __future__ import print_function
import os, sys, glob, re, argparse
import NMEA_Fonctions
import timeit

##  https://nmeachecksum.eqth.net/
##  $   PFEC,GPint,RMC05   * -> $PFEC,GPint,RMC05*2D
##  $   PINFO,Pen Lann 47.51021, -2.50187 --> N47°30.6126 / W002°30.1122,  * -> $PINFO,Pen Lann 47.51021, -2.50187 --> N47°30.6126 / W002°30.1122*22
##  https://github.com/semuconsulting/pynmeagps <- Chercher dans ce depot !
##  https://code.activestate.com/recipes/576789-nmea-sentence-checksum/ <- plan B
##  https://gist.github.com/MattWoodhead/0bc2b3066796e19a3a350689b43b50ab
##  https://rietman.wordpress.com/2008/09/25/how-to-calculate-the-nmea-checksum/


## Calculer le checksum d'une phrase NMEA

## Verifier le checksum d'une phrase NMEA

