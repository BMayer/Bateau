#!/python
# -*- coding: utf-8 -*-
from __future__ import print_function
import os, sys, glob, re, argparse
#from decimal import Decimal

"""
Les fonctions de conversion, de coordonnees geographiques, de temps
ubi bene, ibi patria
"""

'''
== Les formats latitude longitude

Le format NMEA des longitudes / latitudes est 4729.9454,N,00228.0680,W  (dd[d]mm.mmmmm)
Le format DMM des longitudes / latitudes est 47.nnnnn, -2.nnnnn
Par exemple, la latitude NMEA 4729.9454,N est à comprendre comme 
DD 47.49909
DMM N47°29.9454
DMS N47°29'56.724"
et la longitude 00228.0692,W est à comprendre comme 
DD -2.46782
DMM W002°28.0692
DMS W2°28'4.152"
2503.6319    : Latitude exprimée en ddmm.mmmmm: 25° 03.6319' = 25° 03' 37,914"
12136.0099  9: Longitude exprimée en dddmm.mmmmm: 121° 36.0099' = 121° 36' 00,594"

NMEA format is ddmm.mmmm, n/s (d)ddmm.mmmm, e/w
To get to decimal degrees from degrees ad minutes, you use the following formula:
(d)dd + (mm.mmmm/60) (* -1 for W and S)

http://www.hiddenvision.co.uk/ez/[]

https://www.numworks.com/fr/professeurs/activites-pedagogiques/snt/trame-gps/[]

https://fr.wikipedia.org/wiki/NMEA_0183[]

https://stackoverflow.com/questions/36254363/how-to-convert-latitude-and-longitude-of-nmea-format-data-to-decimal[]

http://tvaira.free.fr/bts-sn/activites/activite-peripherique-usb/conversions.html[]

https://www.sunearthtools.com/dp/tools/conversion.php?lang=fr[]

http://family.mayer.free.fr/bateau/conversion_DMS_DMM_DD/Copie%20de%20calculators.htm[]

https://www.gpsvisualizer.com/calculators[]

'''

def convertGeoNmeaToDD(coordonnesNmea) :
    # 4405.2015 —-> 44 + 05.2015/60 = 44,0867 degres
    # Les coordonnees Sud et Ouest seront de valeur negatives.
    ## On recoit 1 ou 2 coordonnees a convertir, on le determine en comptant les virgules.

    aNmea = coordonnesNmea.split(',')
    nbrTokens = len(aNmea)
    if (nbrTokens in (2, 4)) :
        (D, d) = aNmea[0].split('.')
        degres  = D[:-2]
        degres = int(degres)
        minutes = float(D[-2:] + '.' + d) / 60
        sOut = str(degres + minutes)
        #print(degres, minutes)
        if (aNmea[1].upper() in ('S', 'W')) :
            sOut = '-' + sOut
        if (nbrTokens == 2) :
            return sOut
        elif (nbrTokens == 4) :
            (D, d) = aNmea[2].split('.')
            degres  = D[:-2]
            degres = int(degres)
            minutes = float(D[-2:] + '.' + d) / 60
            sOut4 = str(degres + minutes)
            #print(degres, minutes)
            if (aNmea[3].upper() in ('S', 'W')) :
                sOut4 = '-' + sOut4
            return sOut + "\t" + sOut4
    else :
        return False


def convertGeoDDToNmea(coordonneesDD: str) :
    ##  On recoit un float ou 2 ; separes par virgule
    ##  que l'on convertit en DD
    coordonneesDD = str(coordonneesDD)
    unOuDeux = coordonneesDD.find(',') # -1 ou 
    if (unOuDeux == -1) :
        DD = coordonneesDD.strip()
        (DDent, DDdec) = DD.split('.')
        DDdec = str(int(DDdec) * 60)
        Nmea = DDent + DDdec[0:2] + '.' + DDdec[2:]
    else :
        (DD, DD2) = coordonneesDD.split(',')
        DD = DD.strip()
        DD2 = DD2.strip()
        (DDent, DDdec) = DD.split('.')
        DDdec = str(int(DDdec) * 60)
        (DD2ent, DD2dec) = DD2.split('.')
        DD2dec = str(int(DD2dec) * 60)
        if (DDent.startswith('-')) :
            DDsign = 'S'
            DDent = DDent[1:]
        else :
            DDsign = 'N'
        if (DD2ent.startswith('-')) :
            DD2sign = 'W'
            DD2ent = DD2ent[1:]
        else :
            DD2sign = 'E'           
        DDent = DDent.zfill(2)
        DD2ent = DD2ent.zfill(3)
        # Ils semblent que les lat / lon soient pourvues de 4 decimales.
        Nmea = DDent + DDdec[0:2] + '.' + DDdec[2:].ljust(4, "0") + ',' + DDsign + ',' + DD2ent + DD2dec[0:2] + '.' + DD2dec[2:].ljust(4, "0") + ',' + DD2sign
    
    return Nmea

 


def main() :
    print("into converter.py")


if __name__ == '__main__':
    main()
