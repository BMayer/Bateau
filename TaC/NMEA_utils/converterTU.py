#!/python
# -*- coding: utf-8 -*-
from __future__ import print_function
import os, sys, glob, re, argparse

from converter import *
## verif par http://www.hiddenvision.co.uk/ez/

#print("4 ?", convertGeoNmeaToDD("4729.9454,N,00228.0680,W"))

#print("2 ?", convertGeoNmeaToDD("4729.9454,N"))


#2503.6319    : Latitude exprimée en ddmm.mmmmm: 25° 03.6319' = 25° 03' 37,914"
#12136.0099  9: Longitude exprimée en dddmm.mmmmm: 121° 36.0099' = 121° 36' 00,594"

print("25.060531   &   121.600165")
print(convertGeoNmeaToDD("2503.6319,N,12136.0099,E"))

print("-25.060531   &   -121.600165")
print(convertGeoNmeaToDD("2503.6319,S,12136.0099,W"))

# 4727.915,N,00237.362,W
print("4727.915,N,00237.362,W --> 47.46525 -2.6227")
print(convertGeoNmeaToDD("4727.915,N,00237.362,W"))

# 4727.916,N,00238.157,W
print("4727.916,N,00238.157,W --> 47.465266 -2.63595")
print(convertGeoNmeaToDD("4727.916,N,00238.157,W"))

# 4732.517,N,00253.814,W
print("4732.517,N,00253.814,W --> 47.54195 -2.8969")
print(convertGeoNmeaToDD("4732.517,N,00253.814,W"))



####
# Arzal, ponton visiteurs (Garmin152h.nmea)
# 4729.9205,N,00222.6770,W --> 47.498675, -2.37795
# 47°29'55.2"N 2°22'40.6"W
# 47.498675, -2.377950
####

print("47.498675,-2.377950 --> 4729.9205,N,00222.6770,W")
print(convertGeoDDToNmea("47.498675,-2.37795"))

print("47.498675, -2.377950 --> 4729.9205,N,00222.6770,W")
print(convertGeoDDToNmea("47.498675, -2.377950"))
