#!/python
# -*- coding: utf-8 -*-
from __future__ import print_function
import os, sys, glob, re, argparse
from NMEA_Fonctions import *

"""
Supprime les lignes du fichier, suivant les parametres
"""

'''
ALR
APB
BOD
BWC
BWR
IIDBT profondeur $IIDBT,23.3,f,7.1,M,,*4D
IIDPT profondeur $IIDPT,7.1,,*44
GBS
GGA
IIGLL gps $IIGLL,4730.0150,N,00222.9203,W,154959,A,A*49
GSA
GSV
IIHDG cap compas $IIHDG,165,,,,*55
IIHDM cap compas $IIHDM,165,M*3E
HDT
MTA
IIMTW temperature eau $IIMTW,25.1,C*15
IIMWD wind direction and speed $IIMWD,,T,292,M,8.9,N,4.6,M*7E
IIMWV wind speed and angle $IIMWV,127,R,9.0,N,A*2E vent au 127 relatif, 9.0 Noeuds A pour valid
RMB
IIRMC gps $IIRMC,155026,A,4730.0151,N,00222.9205,W,,230623,,,A*53
RME
RMM
RMZ
IIRSA angle de barre $IIRSA,-1.3,A,,V*56 starboard ou port si -
RTE
TXT
VDM
VDO
IIVHW Water Speed and Heading $IIVHW,,T,142,M,1.45,N,2.69,K*6F cap au 142 magnetic a 1.45knt (2.69kmh)
IIVLW Distance Traveled through Water $IIVLW,129.74,N,30.74,N*74
IIVTG Track Made Good and Ground Speed $IIVTG,139,T,,M,1.3,N,2.5,K,A*0A
IIVWR Relative Wind Speed and Angle (vent apparent) $IIVWR,126,R,8.7,N,4.5,M,16.1,K*6A
IIVWT True Wind Speed and Angle (vent vrai) $IIVWT,125,R,8.7,N,4.5,M,16.1,K*6F
IIXDR Transducer Values $IIXDR,U,12.001,V,BatWiFi*17 $IIXDR,C,28.8,C,AirTemp*24
WPL
XTE
IIZDA Time & Date - UTC $IIZDA,155026,23,06,2023,,*5E
PNKEV proprietaire NKE
PNKEP 
PGR proprietaire Garmin
'''

sep = "\t"
geoRmcExcludeList = list()
geoGllExcludeList = list()





def buildZonesList(argsList) :
    ## Examiner les parametres fournis, geoRmcExclude et geoGllExclude
    ## Les verifier puis les convertir en DD si besoin
    ## Toujours 4 points, 3 virgules pour DD ou 7 pour NMEA
    ## Separer les 2 points par un pipe | pour faciliter/optimiser le test de presence dans la zone
    tmpList = list()
    for c in argsList :
        if (c.count(r'.') == 4) :
            nbrComma = c.count(r',')
            if (nbrComma == 7) :
                (c1, c2, c3, c4, c5, c6, c7, c8) = c.split(r',')
                cNW = convertGeoNmeaToDD(c1 + r',' + c2 + r',' + c3 + r',' + c4)
                cSE = convertGeoNmeaToDD(c5 + r',' + c6 + r',' + c7 + r',' + c8)
                tmpList.append(cNW + r'|' + cSE)
            elif (nbrComma == 3) :
                (c1, c2, c3, c4) = c.split(r',')
                tmpList.append(c1 + r',' + c2 + r'|' + c3 + r',' + c4)
            else :
                printErr("Erreur 2 lors de l'examen de la zone [" + str(c) + "]")
        else :
            printErr("Erreur 1 lors de l'examen de la zone [" + str(c) + "]")
    return tmpList


def readInfile(infile, args) :
    #reNmeaDebut = re.compile(r'[\$|!]{1}')
    #reNmeaFin = re.compile(r'\*[0-9A-Fa-f]{2}') 
    #reNmea = re.compile(r'[\$|!]{1}.*\*[0-9A-Fa-f]{2}') 
    f = open(infile, 'r')
    numLigne = 0
    isInsideRmcExcludeZone = False
    isInsideGllExcludeZone = False
    for line in f :
        numLigne += 1
        ##  Examen de la ligne
        posFirstComma = line.find(r',')
        if (posFirstComma > -1) :
            nmeaWord = line[3:posFirstComma].upper()
            ##  Phrase proprietaire 
            ##  On garde pour DEBUG (contient des infos)
            if (line[0:2] == r'$P') :
                printErr(line.strip())
            if (nmeaWord == 'GLL' and not isInvalidGLL(line)) :
                aNmea = line.split(r',')
                point = convertGeoNmeaToDD(r','.join((aNmea[1], aNmea[2], aNmea[3], aNmea[4])))
                printErr("point :", point, "et geoGllExcludeList", geoGllExcludeList)
                if (isThisPointInZone(point, geoGllExcludeList)) :
                    isInsideGllExcludeZone = True
                    printErr("Le point", point, "est DEDANS\n")
                else :
                    isInsideGllExcludeZone = False
                    printErr("Le point", point, "est dehors\n")
            if (nmeaWord == 'RMC' and not isInvalidRMC(line)) :
                aNmea = line.split(r',')
                point = convertGeoNmeaToDD(r','.join((aNmea[3], aNmea[4], aNmea[5], aNmea[6])))
                printErr("point :", point, "et geoRmcExcludeList", geoRmcExcludeList)
                if (isThisPointInZone(point, geoRmcExcludeList)) :
                    isInsideRmcExcludeZone = True
                    printErr("Le point", point, "est DEDANS\n")
                else :
                    isInsideRmcExcludeZone = False
                    printErr("Le point", point, "est dehors\n")
        ##
        if (not isInsideGllExcludeZone and not isInsideRmcExcludeZone) :
            print(line.strip())
            printErr("garder ", line.strip())
        else :
            print("$PINFO,ZoneExclusion,*00")
            printErr("VIRER ", line.strip())

    f.close()


def start() :
    parser = argparse.ArgumentParser(
        'File parser',
        description='PyParse - The File Processor',
        epilog='Thank you for choosing PyParse!',
        #add_help=False,
        )
    parser.add_argument('-i', '--infile', required=True, help="Input file for cleaning")
    parser.add_argument('--geoRmcExclude', action='append', help="suppress zone decimals coordonates")
    parser.add_argument('--geoGllExclude', action='append', help="suppress zone decimals coordonates")
    parser.add_argument('-p', '--info', action='store_true', help="ajout de phrases $PINFO lors des suppresions")
    parser.add_argument('--invert', action='store_true', help="exclure devient ne prendre que ces zones")
    #parser.add_argument('--RMC', action='store_true', help="suppress RMC sentences")

    args = parser.parse_args()
    return args


def main():
    global geoRmcExcludeList
    global geoGllExcludeList
    args = start()

    if args.infile :
        # Traitement des coordonnees fournies
        if (args.geoRmcExclude is not None) :
            geoRmcExcludeList = buildZonesList(args.geoRmcExclude)
        if (args.geoGllExclude is not None) :
            geoGllExcludeList = buildZonesList(args.geoGllExclude)
        printErr("Les zones RMC a exclure :", geoRmcExcludeList)
        printErr("Les zones GLL a exclure :", geoGllExcludeList)
        
        readInfile(args.infile, args)
        
        #printErr(args)
    else :
        quit(sys.exit(1))

    

if __name__ == '__main__':
    main()
