#!/usr/bin/bash

if [[ ${USER} == 'bernard' ]]; then
    pythonRun="python3"
    homeDir="/home/bernard/repoGit/Bateau/TaC/NMEA_utils"
    fileIn="../Traces/echantillonSuppressGeozone.nmea"
else
    pythonRun="/c/Logiciels/Anaconda3/python"
    homeDir="/c/AncienDisqueD/RepoS/Bateau/TaC/NMEA_utils"
    fileIn="../Traces/echantillonSuppressGeozone.nmea"
fi

if [[ -d ${homeDir} && -r ${homeDir}/${fileIn} ]]; then
    cd ${homeDir}
    ##  Ne conserver que le NMEA
    ${pythonRun} ${homeDir}/NMEA_cleaner.py ${homeDir}/${fileIn} \
        1> ${homeDir}/${fileIn}.cleaned \
        2> ${homeDir}/${fileIn}.cleaned.log
        ##  Supprimer les phrases NMEA suivant conditions
        ##  GPS invalide, checksum invalide
        if [[ $? -eq 0 ]]; then
            ${pythonRun} ${homeDir}/NMEA_suppressSentence.py -i ${homeDir}/${fileIn}.cleaned  --AIS --DBT --DPT --HDG \
                1> ${homeDir}/${fileIn}.suppressSentence \
                2> ${homeDir}/${fileIn}.suppressSentence.log
            if [[ $? -eq 0 ]]; then
                # 47.51100,-2.413300,-47.49000,2.36000  -->  4730.66,N,00224.798,W,4729.4,S,00221.6,E
                # 4729.9471,N,00228.0705,W,4744.4444,S,00122.333,E  -->  47.49911833333, -2.46784166667, -47.74074, 1.37221666667
                ${pythonRun} ${homeDir}/NMEA_suppressGeozone.py -i ${homeDir}/${fileIn}.suppressSentence \
                    --geoGllExclude 47.520,-2.5,47.49,-2.38 \
                    1> ${homeDir}/${fileIn}.suppressGeozone \
                    2> ${homeDir}/${fileIn}.suppressGeozone.log
                if [[ $? -ne 0 ]]; then
                    echo "Pb avec suppressGeozone"
                    exit 1
                fi
            else
                echo "Pb avec suppressSentence"
                exit 1
            fi
        else
            echo "Pb avec cleaner"
            exit 1
        fi

else
    echo "Pb avec [${homeDir}/${fileIn}]" > /dev/stderr
    exit 1
fi



exit 0

                    --geoRmcExclude 47.51100,-2.413300,-47.49000,2.36000  \
                    --geoRmcExclude 4729.9471,N,00228.0705,W,4744.4444,S,00122.333,E \
                    --geoGllExclude "truc avec espaces" \
                    --geoRmcExclude 47.520,-2.5,47.49,-2.38 \

