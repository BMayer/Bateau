#!/usr/bin/bash

###
# J'explore un repertoire (et les sous-repertoires)
# action(s) pour chaque fichier
###

if [ ${USER} == 'bernard' ]; then
    dirHome="/home/bernard/repoGit/Bateau/TaC/NMEA_utils/"
    dirTrace="${dirHome}/../Traces/"
else
    dirHome="/D/jenesaispas"
    dirTrace="/D/jenesaispas"
fi

cd ${dirTrace}
#find ${dirTrace} -type f \(-iname *.nmea -o -iname *.log -o -iname *.txt \) -print
for f in *
do
    #isTextFile=$(file --brief ${f} | grep -E "ASCII text|CSV text|data|ISO-8859 text|UTF\-8 Unicode text" --ignore-case --count )
    #isTextFile=$(file --brief ${f} | grep -E "ASCII text|CSV text|data" --ignore-case --count )
    isTextFile=666
    fileBrief=$(file --brief ${f})
    charset=$(file --brief --mime ${f})
    # ASCII text + text/plain; charset=us-ascii
    # ASCII text, with CRLF, CR line terminators + text/plain; charset=us-ascii
    # ASCII text, with CRLF, LF line terminators + text/plain; charset=us-ascii
    # ASCII text, with CRLF line terminators + text/plain; charset=us-ascii
    # ASCII text, with very long lines, with CRLF line terminators + text/plain; charset=us-ascii
    # CSV text + application/csv; charset=us-ascii
    # data + application/octet-stream; charset=binary
    # ISO-8859 text, with CRLF line terminators + text/plain; charset=iso-8859-1
    # Non-ISO extended-ASCII text, with CRLF line terminators + text/plain; charset=unknown-8bit
    # PNG image data, 1669 x 730, 8-bit/color RGB, non-interlaced + image/png; charset=binary
    # PNG image data, 871 x 397, 8-bit/color RGB, non-interlaced + image/png; charset=binary
    # Zip archive data, at least v2.0 to extract + application/zip; charset=binary

    rm --force ${dirTrace}/log/${f}.log
    if [ ${charset:(-8)} == 'us-ascii' ]; then
        python3 ${dirHome}/NMEA_cleaner.py "${f}" 1>> ${dirHome}/concatNmea.tmp 2>> ${dirTrace}/log/${f}.log
    else
        #echo ":-(   ${f}" #>> ${dirHome}/pbDetectionDeType.txt
        echo "Le charset n'est pas celui attendu (${charset} au lieu de us-ascii) " >> ${dirTrace}/log/${f}.log
    fi
done
