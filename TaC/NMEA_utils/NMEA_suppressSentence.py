#!/python
# -*- coding: utf-8 -*-
from __future__ import print_function
import os, sys, glob, re, argparse

"""
Supprime les lignes du fichier, suivant les parametres
"""

'''
ALR
APB
BOD
BWC
BWR
IIDBT profondeur $IIDBT,23.3,f,7.1,M,,*4D
IIDPT profondeur $IIDPT,7.1,,*44
GBS
GGA
IIGLL gps $IIGLL,4730.0150,N,00222.9203,W,154959,A,A*49
GSA
GSV
IIHDG cap compas $IIHDG,165,,,,*55
IIHDM cap compas $IIHDM,165,M*3E
HDT
MTA
IIMTW temperature eau $IIMTW,25.1,C*15
IIMWD wind direction and speed $IIMWD,,T,292,M,8.9,N,4.6,M*7E
IIMWV wind speed and angle $IIMWV,127,R,9.0,N,A*2E vent au 127 relatif, 9.0 Noeuds A pour valid
RMB
IIRMC gps $IIRMC,155026,A,4730.0151,N,00222.9205,W,,230623,,,A*53
RME
RMM
RMZ
IIRSA angle de barre $IIRSA,-1.3,A,,V*56 starboard ou port si -
RTE
TXT
VDM
VDO
IIVHW Water Speed and Heading $IIVHW,,T,142,M,1.45,N,2.69,K*6F cap au 142 magnetic a 1.45knt (2.69kmh)
IIVLW Distance Traveled through Water $IIVLW,129.74,N,30.74,N*74
IIVTG Track Made Good and Ground Speed $IIVTG,139,T,,M,1.3,N,2.5,K,A*0A
IIVWR Relative Wind Speed and Angle (vent apparent) $IIVWR,126,R,8.7,N,4.5,M,16.1,K*6A
IIVWT True Wind Speed and Angle (vent vrai) $IIVWT,125,R,8.7,N,4.5,M,16.1,K*6F
IIXDR Transducer Values $IIXDR,U,12.001,V,BatWiFi*17 $IIXDR,C,28.8,C,AirTemp*24
WPL
XTE
IIZDA Time & Date - UTC $IIZDA,155026,23,06,2023,,*5E
PNKEV proprietaire NKE
PNKEP 
PGR proprietaire Garmin
'''

sep = "\t"

def isInvalidGLL(l) :
    a = l.split(',')
    #print("GLL invalid ?", a[6])
    if (a[6].upper() == 'A') :
        return False
    else :
        return True

def isInvalidRMC(l) :
    a = l.split(',')
    #print("RMC invalid ?", a[2])
    if (a[2].upper() == 'A') :
        return False
    else :
        return True

def readInfile(infile, args) :
    #reNmeaDebut = re.compile(r'[\$|!]{1}')
    #reNmeaFin = re.compile(r'\*[0-9A-Fa-f]{2}') 
    #reNmea = re.compile(r'[\$|!]{1}.*\*[0-9A-Fa-f]{2}') 
    f = open(infile, 'r')
    numLigne = 0
    for line in f :
        numLigne += 1
        ##  Examen de la ligne
        ##  Suppression AIS ?
        if (args.AIS and line[0] == '!') :
            printErr("del AIS " + str(numLigne))
            continue
        ##  Phrase proprietaire ?
        # TODO
        # if (line[0:1] == '$P') :
        #     if (args.prop) :
        #         printErr("del prop " + str(numLigne))
        #         continue
        else :
            # Si besoin nmeaTalker = line[1:3]
            posFirstComma = line.find(r',')
            if (posFirstComma > -1) :
                nmeaWord = line[3:posFirstComma].upper()
                #print(nmeaWord, ' <-- ', line.strip())
                if (nmeaWord == 'GLL') :
                    if (args.GLL) :
                        printErr("del GLL " + str(numLigne))
                        continue
                    elif (args.invalidGLL and isInvalidGLL(line)) :
                        printErr("del invalidGLL " + str(numLigne))
                        continue
                if (nmeaWord == 'RMC') :
                    if (args.RMC) :
                        printErr("del RMC " + str(numLigne))
                        continue
                    elif (args.invalidRMC and isInvalidRMC(line)) :
                        printErr("del invalidRMC " + str(numLigne))
                        continue
                if (nmeaWord == 'DBT' and args.DBT) :
                    printErr("del DBT " + str(numLigne))
                    continue
                if (nmeaWord == 'DPT' and args.DPT) :
                    printErr("del DPT " + str(numLigne))
                    continue
                if (nmeaWord == 'HDG' and args.HDG) :
                    printErr("del HDG " + str(numLigne))
                    continue
                if (nmeaWord == 'HDM' and args.HDM) :
                    printErr("del HDM " + str(numLigne))
                    continue
                if (nmeaWord == 'MTW' and args.MTW) :
                    printErr("del MTW " + str(numLigne))
                    continue
                if (nmeaWord == 'MWD' and args.MWD) :
                    printErr("del MWD " + str(numLigne))
                    continue
                if (nmeaWord == 'MWV' and args.MWV) :
                    printErr("del MWV " + str(numLigne))
                    continue
            #print(nmeaWord, ' <-- ', line.strip())
            print(line.strip())

    f.close()


def printErr(*args, **kwargs) :
    print(*args, file=sys.stderr, **kwargs)


def start() :
    parser = argparse.ArgumentParser(
        'File parser',
        description='PyParse - The File Processor',
        epilog='Thank you for choosing PyParse!',
        #add_help=False,
        )
    parser.add_argument('-i', '--infile', required=True, help="Input file for cleaning")
    parser.add_argument('--RMC', action='store_true', help="suppress RMC sentences")
    parser.add_argument('--invalidRMC', action='store_true', help="suppress RMC sentences invalids")
    parser.add_argument('--GLL', action='store_true', help="suppress GLL sentences")
    parser.add_argument('--invalidGLL', action='store_true', help="suppress GLL sentences invalids")
    parser.add_argument('-a', '--AIS', action='store_true', help="suppress AIS sentences")
    parser.add_argument('-p', '--prop', action='store_true', help="suppress proprietaries sentences")
    parser.add_argument('--DBT', action='store_true', help="suppress DBT sentences")
    parser.add_argument('--DPT', action='store_true', help="suppress DPT sentences")
    parser.add_argument('--HDG', action='store_true', help="suppress HDG sentences")
    parser.add_argument('--HDM', action='store_true', help="suppress HDM sentences")
    parser.add_argument('--MTW', action='store_true', help="suppress MTW sentences")
    parser.add_argument('--MWD', action='store_true', help="suppress MWD sentences")
    parser.add_argument('--MWV', action='store_true', help="suppress MWV sentences")


    #parser.add_argument('-o', '--outfile', help="Cleaned output file")
    args = parser.parse_args()
    return args


def main():
    args = start()

    if args.infile :
        # Traitement
        readInfile(args.infile, args)
        quit(0)
    #print(args)

    

if __name__ == '__main__':
    main()
