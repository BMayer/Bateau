#!/usr/bin/bash

##  Compter les decimales des coordonnees geo des phrases RMC

# $IIGLL,4727.915,N,00237.362,W,105238,A,A*45
# $ECRMC,212650,A,4730.939,N,00230.994,W,-1.#IO,-1.#IO,090319,0.666,W*67
# $GPRMC,165230.00,A,4729.97727,N,00222.58464,W,0.078,,050619,,,A*68

homeDir=/home/bernard/repoGit/Bateau/TaC/Traces
touch ${homeDir}/xtrRmc.NS.out
touch ${homeDir}/xtrRmc.EW.out

for f in $(ls ${homeDir}/*.nmea && ls ${homeDir}/*.log); do
    #echo ${f}
    # N/S
    grep 'RMC' ${f} | cut -d ',' -f 4 | cut -d '.' -f 2 >> ${homeDir}/xtrRmc.NS.out
done

for f in $(ls ${homeDir}/*.nmea && ls ${homeDir}/*.log); do
    #echo ${f}
    # E/W
    grep 'RMC' ${f} | cut -d ',' -f 6 | cut -d '.' -f 2 >> ${homeDir}/xtrRmc.EW.out
done

