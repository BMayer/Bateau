#!/python
# -*- coding: utf-8 -*-
from __future__ import print_function
import os, sys, glob, re, argparse

import math
from converter import *
import timeit

## Comment savoir si une coordonnées géographique est dans une zone ou pas ?

def pointIsInZone(point, zones) :
    #global zones

    (latPoint, lonPoint) = point.split(',')
    for zone in zones :
        (zoneNW, zoneSE) = zone.split('|')
        (latNW, lonNW) = zoneNW.split(',')
        (latSE, lonSE) = zoneSE.split(',')
        print(latPoint, lonPoint, "is in", latNW, lonNW, latSE, lonSE, "?")
        if (float(latPoint) <= float(latNW)) :
             if (float(lonPoint) >= float(lonNW)) :
                if (float(latPoint) >= float(latSE)) :
                    if (float(lonPoint) <= float(lonSE)) :
                        return True
        return False

tsStart = timeit.timeit()
print("##  QUADRANT Nord Est, nombres float uniquement positifs")
zones = list()
zones.append("2.0,3.0|1.0,4.0")
points = list()
points.append("0.5,2.5")
points.append("0.5,3.5")
points.append("0.5,4.5")
points.append("1.5,2.5")
points.append("1.5,3.5") # True
points.append("1.5,4.5")
points.append("2.5,2.5")
points.append("2.5,3.5")
points.append("2.5,4.5")
for point in points :
    if (pointIsInZone(point, zones)) :
        print("point", point, "est dans la zone")
    else :
        print("! point", point, "n'est pas dans la zone")

print("##  QUADRANT SW, nombres float uniquement negatifs")
zones = list()
zones.append("-1.0,-4.0|-2.0,-3.0")
points = list()
points.append("-0.5,-4.5")
points.append("-0.5,-3.5")
points.append("-0.5,-2.5")
points.append("-1.5,-4.5")
points.append("-1.5,-3.5") # True
points.append("-1.5,-2.5")
points.append("-2.5,-4.5")
points.append("-2.5,-3.5")
points.append("-2.5,-2.5")
for point in points :
    if (pointIsInZone(point, zones)) :
        print("point", point, "est dans la zone")
    else :
        print("! point", point, "n'est pas dans la zone")

print("##  QUADRANT NW, latitudes + et longitudes -")
zones = list()
zones.append("2.0,-4.0|1.0,-3.0")
points = list()
points.append("2.5,-4.5")
points.append("2.5,-3.5")
points.append("2.5,-2.5")
points.append("1.5,-4.5")
points.append("1.5,-3.5") # True
points.append("1.5,-2.5")
points.append("0.5,-4.5")
points.append("0.5,-3.5")
points.append("0.5,-2.5")
for point in points :
    if (pointIsInZone(point, zones)) :
        print("point", point, "est dans la zone")
    else :
        print("! point", point, "n'est pas dans la zone")

print("##  QUADRANT SE, latitudes + et longitudes -")
zones = list()
zones.append("-1.0,3.0|-2.0,4.0")
points = list()
points.append("-0.5,4.5")
points.append("-0.5,3.5")
points.append("-0.5,2.5")
points.append("-1.5,4.5")
points.append("-1.5,3.5") # True
points.append("-1.5,2.5")
points.append("-2.5,4.5")
points.append("-2.5,3.5")
points.append("-2.5,2.5")
for point in points :
    if (pointIsInZone(point, zones)) :
        print("point", point, "est dans la zone")
    else :
        print("! point", point, "n'est pas dans la zone")

tsStop = timeit.timeit()
print("Duree :", tsStop - tsStart)
