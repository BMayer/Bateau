#!/usr/bin/bash

## Supprimer l'horodatage de debut de ligne dans un fichier log NKE
##
##==> Box WiFi V2.1  Start logging
## 0.418 $PNKEV,Box WiFi nke 3,V2.1,Aug 23 2017,17:04:14,00.1E.C0.39.41.44,V1.0*04
## 1.457 $IIGLL,4730.1826,N,00223.1864,W,,A,A*47
## 15986.008 $IIRMC,172525,A,4730.0168,N,00222.9174,W,,050624,,,A*5C
##==> Box WiFi V2.1  Stop logging
## devient
## $PNKEV,Box WiFi nke 3,V2.1,Aug 23 2017,17:04:14,00.1E.C0.39.41.44,V1.0*04
## $IIGLL,4730.1826,N,00223.1864,W,,A,A*47
## $IIRMC,172525,A,4730.0168,N,00222.9174,W,,050624,,,A*5C

fileIn=$1

if [[ ! -e ${fileIn} ]]; then
    echo "Fichier [${fileIn}] absent"
    exit 1
#else
    #echo "Traitement de ${fileIn}"
fi

while read -r line; do
  echo "\$${line##*\$}"
done <${fileIn}
